const express = require("express");

const mongoose = require("mongoose");

const app = express();
const port = 4001;

mongoose.connect(`mongodb+srv://aronndc:admin123@zuitt-batch-197.lzwxz7u.mongodb.net/S35-C1?retryWrites=true&w=majority`, {
	useNewUrlParser: true,
	useUnifiedTopology: true
});

let db = mongoose.connection

db.on('error', console.error.bind(console, "Connection"));
db.once('open', () => console.log('Connection to MongoDB!'));


/*
USER
*/


const userSchema = new mongoose.Schema({
	firstName: String,
	lastName: String,
    username: String,
    password: String,
    email: String

});

const User = mongoose.model('User', userSchema);

app.use(express.json());
app.use(express.urlencoded({extended: true}));


app.post('/register', (req, res) => {
    User.findOne({ firstName: req.body.firstName }, (error, result) => {
        if (error) {
            return res.send(error)
        } else if (result != null && result.firstName == req.body.firstName) {
            return res.send('Duplicate User Found!')
        } else {
            let newUser = new User({
                firstName: req.body.firstName,
                lastName: req.body.lastName,
                username: req.body.username,
                password: req.body.password,
                email: req.body.email
            })

            newUser.save((error, savedUser) => {
                if (error) {
                    return console.error(error)
                } else {
                    return res.status(201).send('New user Created!')
                }
            })
        }
    })
});

app.get('/users', (req, res) => {
    User.find({}, (error, result) => {
        if(error){
            return res.send(error)
        } else {
            return res.status(200).json({
                users: result
            })
        }
    })
});


/*
 PRODUCT
*/


const productSchema = new mongoose.Schema({
    name: String,
    description: String,
    price: Number

});

const Product = mongoose.model('Product', productSchema);

app.use(express.json());
app.use(express.urlencoded({extended: true}));


app.post('/createproduct', (req, res) => {
    Product.findOne({ name: req.body.name }, (error, result) => {
        if (error) {
            return res.send(error)
        } else if (result != null && result.name == req.body.name) {
            return res.send('Duplicate Product Found!')
        } else {
            let newProduct = new Product({
                name: req.body.name,
                description: req.body.description,
                price: req.body.price
                
            })

            newProduct.save((error, savedProduct) => {
                if (error) {
                    return console.error(error)
                } else {
                    return res.status(201).send('New Product Created!')
                }
            })
        }
    })
});

app.get('/products', (req, res) => {
	Product.find({}, (error, result) => {
		if(error){
			return res.send(error)
		} else {
			return res.status(200).json({
				users: result
			})
		}
	})
});

app.listen(port, () => console.log(`Server is running at port ${port}`)); 